import dataGlasses from "./data.js";

const getEle = (id) => document.getElementById(id);
let selected;

const _findIndex = (id) => {
  return dataGlasses.findIndex((item) => {
    return item.id === id;
  });
};

const handleWearGlasses = (id) => {
  const index = _findIndex(id);
  if (index !== -1) {
    getEle("avatar").innerHTML = `
    <img src="${dataGlasses[index].virtualImg}" />
    `;
    getEle("glassesInfo").innerHTML = `
    <h5>${dataGlasses[index].name} - ${dataGlasses[index].brand} (${dataGlasses[index].color}) </h5>
    <div class="bg-danger d-inline-block p-1">$${dataGlasses[index].price}</div>
    <span class="text-success">Stocking</span>
    <p class="mt-3">${dataGlasses[index].description}</p>
    `;
  }
  getEle("glassesInfo").style.display = "block";
  selected = index;
};

const renderGlassesList = () => {
  let glassesHTML = "";
  dataGlasses.forEach((item) => {
    glassesHTML += `
      <img
    class="col-4 vglasses__items"
    src="${item.src}"
    onclick="handleWearGlasses('${item.id}')"
  />`;
  });
  getEle("vglassesList").innerHTML = glassesHTML;
};

renderGlassesList();
window.handleWearGlasses = handleWearGlasses;

const removeGlasses = (value) => {
  if (value == false) {
    selected -= 1;
    if (selected < 0) {
      selected = dataGlasses.length - 1;
      handleWearGlasses(dataGlasses[selected].id);
    } else {
      handleWearGlasses(dataGlasses[selected].id);
    }
  }
  if (value == true) {
    selected += 1;
    if (selected >= dataGlasses.length) {
      selected = 0;
      handleWearGlasses(dataGlasses[selected].id);
    } else {
      handleWearGlasses(dataGlasses[selected].id);
    }
  }
};

window.removeGlasses = removeGlasses;
